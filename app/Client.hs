module Main where

import           Control.Concurrent
import           Control.Exception
import           Network.HTTP.Client
import           Network.Wai.Handler.Warp
import           Servant
import           Servant.Client
-- import           Client.Example
import           Server.SQLite

postMsg :: Message -> ClientM NoContent
getMsgs :: ClientM [Message]
postMsg :<|> getMsgs = client api

main :: IO ()
main = do
  -- you could read this from some configuration file,
  -- environment variable or somewhere else instead.
  let dbfile = "test.db"

  initDB dbfile

  mgr <- newManager defaultManagerSettings

  bracket (forkIO $ run 5000 $ app dbfile) killThread $ \_ -> do
    ms <-
      flip runClientM (mkClientEnv mgr (BaseUrl Http "127.0.0.1" 5000 "")) $ do
        postMsg "hello"
        postMsg "world"
        getMsgs

    print ms

  -- run
