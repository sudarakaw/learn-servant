module Main where

-- import           Network.Wai.Handler.Warp
-- import           ApiType
-- import           Server.Example
-- import           Server.API
-- import           Server.HTML
-- import           Server.InOut
-- import           Server.Header
-- import           Server.Static
-- import           Server.Nested
-- import           Server.CustomHandler
-- import           Server.Stream
-- import           Server.JavaScript
-- import           Server.Authentication
-- import           Server.AuthGeneralized
-- import           Server.Doc
-- import           Server.SQLite
import           Server.JWT

main :: IO ()
main = do
  -- writeJSFiles

  -- let dbfile = "test.db"

  -- run 5000 $ app dbfile
  -- run 5000 app
  run
