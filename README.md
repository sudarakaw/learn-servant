# Practice Code: Tutorial & Cookbook of servant – A Type-Level Web DSL

Practice code created while learning [Servant](https://haskell-servant.github.io/),
a [Haskell](https://www.haskell.org/) web server framework.
