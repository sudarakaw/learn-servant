{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Client.Example where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Data.Aeson
import           GHC.Generics
import           Network.HTTP.Client            ( defaultManagerSettings
                                                , newManager
                                                )
import           Servant
import           Servant.Client

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

data Position = Position
  { xCoord :: Int
  , yCoord :: Int
  }
  deriving (Show, Generic)

instance FromJSON Position


newtype HelloMessage = HelloMessage {msg::String}
  deriving (Show, Generic)

instance FromJSON HelloMessage


data ClientInfo = ClientInfo
  { name         :: String
  , email        :: String
  , age          :: Int
  , interestedIn :: [String]
  }
  deriving Generic

instance ToJSON ClientInfo

data Email = Email
  { from    :: String
  , to      :: String
  , subject :: String
  , body    :: String
  }
  deriving (Show, Generic)

instance FromJSON Email

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

type API
  = "position" :> Capture "x" Int :> Capture "y" Int :> Get '[JSON] Position
  :<|> "hello" :> QueryParam "name" String :> Get '[JSON] HelloMessage
  :<|> "marketing" :> ReqBody '[JSON] ClientInfo :> Post '[JSON] Email

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

api :: Proxy API
api = Proxy

position :<|> hello :<|> marketing = client api

queries :: ClientM (Position, HelloMessage, Email)
queries = do
  pos <- position 12 34
  message <- hello (Just "servant")
  em <- marketing (ClientInfo "Alp" "alp@foo.com" 26 ["haskell", "mathematics"])

  return (pos, message, em)

run :: IO ()
run = do
  manager <- newManager defaultManagerSettings
  res     <- runClientM queries
                        (mkClientEnv manager (BaseUrl Http "127.0.0.1" 5000 ""))

  case res of
    Left  err                -> putStrLn $ "Error: " ++ show err
    Right (pos, message, em) -> do
      print pos
      print message
      print em

-- }}}
