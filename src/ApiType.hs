{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module ApiType where

import           Data.Text
import           Data.Time
import           Servant.API


-- The endpoint at /users expects a GET request with query string parameter
-- sortby whose value can be one of age or name and returns a list/array of JSON
-- objects describing users, with fields age, name, email, registration_date”.

type UserAPI = "users"
             :> QueryParam "sortby" SortBy
             :> Get '[JSON] [User]

-- NOTE: in order to construct a type for root URL, simple leave the first
-- (string) part(s) at start of the above code

type UserAPI2
              -- GET /users/list-all
              = "users" :> "list-all" :> Get '[JSON] [User]
              -- GET /list-all/users
              :<|> "list-all" :> "users" :> Get '[JSON] [User]


-- `Capture` data from URL
type UserAPI5
    -- GET /user/:userid
    = "user" :> Capture "userid" Integer :> Get '[JSON] User
    -- DELETE /user/:userid
    :<|> "user" :> Capture "userid" Integer :> DeleteNoContent


-- URL Query string access
type UserAPI6
              -- GET /users?sortby=:field
              = "users" :> QueryParam "sortby" SortBy :> Get '[JSON] [User]


-- HTTP POST with JSON body
type UserAPI7
  -- HTTP POST /users ...
  = "users"
  :> ReqBody '[JSON] User
  :> Post '[JSON] User

  -- HTTP PUT /users/:userid
  :<|> "users"
  :> Capture "userid" Integer
  :> ReqBody '[JSON] User
  :> Put '[JSON] User


-- Using HTTP request headers
type UserAPI8
  = "users"
  :> Header "User-Agent" Text
  :> Get '[JSON] [User]


-- Sending HTTP response headers
type UserAPI10
  = "users"
  :> Get '[JSON] (Headers '[Header "User-Count" Integer] [User])


-- HTTP Basic Authantication
type ProtectedAPI
  = UserAPI                                    -- this route is public
  :<|> BasicAuth "api-realm" User :> UserAPI2  -- this route is protected


-- Empty API ???
type UserAPI12 innerAPI
  -- ... /users ...
  = UserAPI
  -- ... /inner/...
  :<|> "inner" :> innerAPI

type UserAPI12Alone = UserAPI12 EmptyAPI

type UserAPI12WithInner = UserAPI12 UserAPI2


data SortBy = Age | Name

data User = User
  { name              :: String
  , age               :: Int
  , email             :: String
  , registration_date :: UTCTime
  }
