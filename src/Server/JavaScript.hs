{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module Server.JavaScript where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import qualified Data.Text.IO                  as T
import           GHC.Generics
import           Servant
import           Servant.JS
import           System.Random

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

data Point = Point
  { xCoord :: Double
  , yCoord :: Double
  }
  deriving Generic

instance ToJSON Point


data Search a = Search
  { query   :: Text
  , results :: [a]
  }
  deriving Generic

instance ToJSON a => ToJSON (Search a)


data Book = Book
  { author :: Text
  , title  :: Text
  , year   :: Int
  }
  deriving Generic

instance ToJSON Book


books :: [Book]
books =
  [ Book
    "Paul Hudak"
    "The Haskell School of Expression: Learning Functional Programming through Multimedia"
    2000
  , Book "Bryan O'Sullivan, Don Stewart, and John Goerzen"
         "Real World Haskell"
         2008
  , Book "Miran Lipovača" "Learn You a Haskell for Great Good!"            2011
  , Book "Graham Hutton"  "Programming in Haskell"                         2007
  , Book "Simon Marlow"   "Parallel and Concurrent Programming in Haskell" 2013
  , Book "Richard Bird"
         "Introduction to Functional Programming using Haskell"
         1998
  ]

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

type API
  = "point" :> Get '[JSON] Point
  :<|> "books" :> QueryParam "q" Text :> Get '[JSON] (Search Book)

type APIWithStatic = API :<|> Raw

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

api :: Proxy API
api = Proxy

apiWithStatic :: Proxy APIWithStatic
apiWithStatic = Proxy

bookMatching :: Text -> Book -> Bool
bookMatching q b =
  lq `T.isInfixOf` T.toLower (author b) || lq `T.isInfixOf` T.toLower (title b)
  where lq = T.toLower q

searchBook :: Monad m => Maybe Text -> m (Search Book)
searchBook Nothing  = return $ Search "" books
searchBook (Just q) = return $ Search q $ filter (bookMatching q) books

randomPoint :: MonadIO m => m Point
randomPoint = liftIO . getStdRandom $ \g ->
  let (rx, g' ) = randomR (-1, 1) g
      (ry, g'') = randomR (-1, 1) g'
  in  (Point rx ry, g'')

server :: Server API
server = randomPoint :<|> searchBook

serverWithStatic :: Server APIWithStatic
serverWithStatic = server :<|> serveDirectoryFileServer "static"

app :: Application
app = serve apiWithStatic serverWithStatic

apiJS :: Text
apiJS = jsForAPI api vanillaJS

writeJSFiles :: IO ()
writeJSFiles = do
  T.writeFile "static/api.js" apiJS

-- }}}
