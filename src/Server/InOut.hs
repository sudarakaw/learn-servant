{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}


module Server.InOut where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Functor
import           GHC.Generics
import           Servant
import           System.Directory

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

newtype FileContent = FileContent
  { content :: String
  }
  deriving Generic

instance ToJSON FileContent

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

type API = "file.txt" :> Get '[JSON] FileContent

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

api :: Proxy API
api = Proxy

server :: Server API
server = do
  exists <- liftIO $ doesFileExist "LICENSE"
  if exists
    then liftIO $ readFile "LICENSE" <&> FileContent
    else throwError custom404Err
 where
  custom404Err = err404
    { errBody = "File just isn't there, please leave this server alone."
    }

app :: Application
app = serve api server

-- }}}
