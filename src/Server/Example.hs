{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeOperators #-}

module Server.Example where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Data.Aeson
import           Data.Time.Calendar
import           GHC.Generics
import           GHC.TypeLits
import           Servant

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

data User = User
  { name              :: String
  , age               :: Int
  , email             :: String
  , registration_date :: Day
  }
  deriving Generic

instance ToJSON User


albert :: User
albert = User "Albert Einstein" 136 "ae@mc2.org" (fromGregorian 1905 12 1)

issac :: User
issac = User "Isaac Newton" 372 "isaac@newton.co.uk" (fromGregorian 1683 3 1)

users1 :: [User]
users1 = [issac, albert]

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

-- GET /users
type UserAPI1 = "users" :> Get '[JSON] [User]

-- type AlbertAPI = "albert" :> Get '[JSON] User
--
-- type IssacAPI = "issac" :> Get '[JSON] User

type UserAPI (name :: Symbol) = name :> Get '[JSON] User

-- type UserAPI2 = UserAPI1 :<|> AlbertAPI :<|> IssacAPI
type UserAPI2 = UserAPI1 :<|> UserAPI "albert" :<|> UserAPI "issac"

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

server1 :: Server UserAPI1
server1 = return users1

userAPI1 :: Proxy UserAPI1
userAPI1 = Proxy

app1 :: Application
app1 = serve userAPI1 server1

server2 :: Server UserAPI2
server2 = return users1 :<|> return albert :<|> return issac

userAPI2 :: Proxy UserAPI2
userAPI2 = Proxy

app2 :: Application
app2 = serve userAPI2 server2

-- }}}
