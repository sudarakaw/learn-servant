{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}


module Server.Nested where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Data.Aeson
import           Data.Time.Calendar
import           GHC.Generics
import           Servant

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

data User = User
  { name              :: String
  , age               :: Int
  , email             :: String
  , registration_date :: Day
  }
  deriving Generic

instance ToJSON User
instance FromJSON User

albert :: User
albert = User "Albert Einstein" 136 "ae@mc2.org" (fromGregorian 1905 12 1)


data Product = Product
  { productId :: Int
  , code      :: String
  }
  deriving Generic

instance ToJSON Product
instance FromJSON Product

item :: Product
item = Product 123 "SKU 873423487"

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

type API1
         -- view the user with given userid, in JSON
         = Capture "userid" Int :> Get '[JSON] User
         -- delete the user with given userid. empty response
         :<|> Capture "userid" Int :> DeleteNoContent

type API2
  = Capture "userid" Int
  :> (Get '[JSON] User :<|> DeleteNoContent)

type UsersAPI
  -- GET .../ list users
  = Get '[JSON] [User]
  -- POST .../ add a user
  :<|> ReqBody '[JSON] User :> PostNoContent
  :<|> Capture "userid" Int
     -- GET .../:userid list single user
  :> (Get '[JSON] User
     -- PUT .../:userid update a single user
     :<|> ReqBody '[JSON] User :> PutNoContent
     -- DELETE .../:userid delete a single user
     :<|> DeleteNoContent
     )

type ProductsAPI
  -- GET .../ list products
  = Get '[JSON] [Product]
  -- POST .../ add a product
  :<|> ReqBody '[JSON] Product :> PostNoContent
  :<|> Capture "userid" Int
     -- GET .../:userid list single product
  :> (Get '[JSON] Product
     -- PUT .../:userid update a single product
     :<|> ReqBody '[JSON] Product :> PutNoContent
     -- DELETE .../:userid delete a single product
     :<|> DeleteNoContent
     )

type CombinedAPI1
  = "users" :> UsersAPI
  :<|> "products" :> ProductsAPI


type APIFor model id
  -- GET .../ list models
  = Get '[JSON] [model]
  -- POST .../ add a model
  :<|> ReqBody '[JSON] model :> PostNoContent
  :<|> Capture "userid" id
     -- GET .../:userid list single model
  :> (Get '[JSON] model
     -- PUT .../:userid update a single model
     :<|> ReqBody '[JSON] model :> PutNoContent
     -- DELETE .../:userid delete a single model
     :<|> DeleteNoContent
     )

type CombinedAPI2
  = "users" :> APIFor User Int
  :<|> "products" :> APIFor Product Int

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

api1 :: Proxy API1
api1 = Proxy

server1 :: Server API1
server1 = getUser :<|> deleteUser
 where
  getUser :: Int -> Handler User
  getUser _ = return albert
  deleteUser :: Int -> Handler NoContent
  deleteUser _ = error "..."

api2 :: Proxy API2
api2 = Proxy

server2 :: Server API2
server2 uid = getUser uid :<|> deleteUser uid
 where
  getUser :: Int -> Handler User
  getUser _ = return albert
  deleteUser :: Int -> Handler NoContent
  deleteUser _ = error "..."


usersServer :: Server UsersAPI
usersServer = getUsers :<|> newUser :<|> userOperations
 where
  getUsers :: Handler [User]
  getUsers = return [albert]

  newUser :: User -> Handler NoContent
  newUser _ = return NoContent

  userOperations uid = viewUser uid :<|> updateUser uid :<|> deleteUser uid

  viewUser :: Int -> Handler User
  viewUser _ = return albert

  updateUser :: Int -> User -> Handler NoContent
  updateUser _ _ = return NoContent

  deleteUser :: Int -> Handler NoContent
  deleteUser _ = return NoContent

productsServer :: Server ProductsAPI
productsServer = getProducts :<|> newProduct :<|> productOperations
 where
  getProducts :: Handler [Product]
  getProducts = return [item]

  newProduct :: Product -> Handler NoContent
  newProduct _ = return NoContent

  productOperations uid =
    viewProduct uid :<|> updateProduct uid :<|> deleteProduct uid

  viewProduct :: Int -> Handler Product
  viewProduct _ = return item

  updateProduct :: Int -> Product -> Handler NoContent
  updateProduct _ _ = return NoContent

  deleteProduct :: Int -> Handler NoContent
  deleteProduct _ = return NoContent

combinedAPI1 :: Proxy CombinedAPI1
combinedAPI1 = Proxy

combinedServer1 :: Server CombinedAPI1
combinedServer1 = usersServer :<|> productsServer


combinedAPI2 :: Proxy CombinedAPI2
combinedAPI2 = Proxy

combinedServer2 :: Server CombinedAPI2
combinedServer2 = usersServer :<|> productsServer


app :: Application
app =
  -- serve api1 server1
  -- serve api2 server2
  -- serve combinedAPI1 combinedServer1
  serve combinedAPI2 combinedServer2

-- }}}
