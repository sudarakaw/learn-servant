{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}


module Server.Header where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Data.Aeson
import           Data.Time.Calendar
import           GHC.Generics
import           Servant

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

data User = User
  { name              :: String
  , age               :: Int
  , email             :: String
  , registration_date :: Day
  }
  deriving Generic

instance ToJSON User


albert :: User
albert = User "Albert Einstein" 136 "ae@mc2.org" (fromGregorian 1905 12 1)


-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

type API
  = Capture "withHeader" Bool
  :> Get '[JSON] (Headers '[Header "X-A-Bool" Bool , Header "X-An-Int" Int] User)

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

api :: Proxy API
api = Proxy

server :: Server API
server withHeader = return $ if withHeader
  then addHeader True $ addHeader 1797 albert
  else noHeader $ noHeader albert

app :: Application
app = serve api server

-- }}}
