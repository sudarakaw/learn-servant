{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}

module Server.AuthGeneralized where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Data.Aeson
import           Data.ByteString
import           Data.Map                       ( Map )
import qualified Data.Map                      as Map
import           Data.Text                      ( Text )
import           GHC.Generics
import           Network.Wai
import           Servant
import           Servant.Server.Experimental.Auth
import           Web.Cookie

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

-- | private data that needs protection
newtype PrivateData = PrivateData {ssshhh::Text}
  deriving Generic

instance ToJSON PrivateData

-- | public data that anyone can use.
newtype PublicData = PublicData { somedata :: Text }
  deriving Generic

instance ToJSON PublicData

-- | An account type that we "fetch from the database" after
-- performing authentication
newtype Account = Account { unAccount :: Text }


-- | A (pure) database mapping keys to accounts.
database :: Map ByteString Account
database = Map.fromList
  [ ("key1", Account "Anne Briggs")
  , ("key2", Account "Bruce Cockburn")
  , ("key3", Account "Ghédalia Tazartès")
  ]

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

-- | a type to wrap our public api
type PublicAPI = Get '[JSON] [PublicData]

-- | a type to wrap our private api
type PrivateAPI = Get '[JSON] PrivateData


type API
  = "public" :> PublicAPI
  :<|> "private" :> AuthProtect "cookie-auth" :> PrivateAPI

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

-- | A method that, when given a password, will return an Account.
-- This is our bespoke (and bad) authentication logic.
lookupAccount :: ByteString -> Handler Account
lookupAccount key = case Map.lookup key database of
  Nothing   -> throwError (err403 { errBody = "Invalid Cookie" })
  Just user -> return user

-- | The auth handler wraps a function from Request -> Handler Account.
-- We look for a token in the request headers that we expect to be in the cookie.
-- The token is then passed to our `lookupAccount` function.
authHandler :: AuthHandler Request Account
authHandler = mkAuthHandler handler
 where
  maybeToEither e = maybe (Left e) Right
  throw401 msg = throwError $ err401 { errBody = msg }
  handler req = either throw401 lookupAccount $ do
    cookie <-
      maybeToEither "Missing cookie header" $ lookup "cookie" $ requestHeaders
        req
    maybeToEither "Missing token in cookie"
      $ lookup "auth-cookie"
      $ parseCookies cookie

-- | We need to specify the data returned after authentication
type instance AuthServerData (AuthProtect "cookie-auth")
  = Account


authContext :: Context (AuthHandler Request Account ': '[])
authContext = authHandler :. EmptyContext

api :: Proxy API
api = Proxy

-- | an implementation of our server. Here is where we pass all the handlers to our endpoints.
-- In particular, for the BasicAuth protected handler, we need to supply a function
-- that takes 'User' as an argument.
server :: Server API
server =
  let publicAPIHandler = return [PublicData "foo", PublicData "bar"]
      privateAPIHandler (Account name) =
        return $ PrivateData $ "This is a secret: " <> name
  in  publicAPIHandler :<|> privateAPIHandler

app :: Application
app = serveWithContext api authContext server

-- }}}
