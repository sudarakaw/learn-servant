{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Server.API where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Data.Aeson
import           Data.List
import           Data.Time.Calendar
import           GHC.Generics
import           GHC.TypeLits
import           Servant

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

data Position = Position
  { xCoord :: Int
  , yCoord :: Int
  }
  deriving Generic

instance ToJSON Position


newtype HelloMessage = HelloMessage {msg::String}
  deriving Generic

instance ToJSON HelloMessage


data ClientInfo = ClientInfo
  { name         :: String
  , email        :: String
  , age          :: Int
  , interestedIn :: [String]
  }
  deriving Generic

instance FromJSON ClientInfo
instance ToJSON ClientInfo

data Email = Email
  { from    :: String
  , to      :: String
  , subject :: String
  , body    :: String
  }
  deriving Generic

instance ToJSON Email


emailForClient :: ClientInfo -> Email
emailForClient c = Email from' to' subject' body'
 where
  from'    = "great@company.com"
  to'      = email c
  subject' = "Hey " ++ name c ++ ", we miss you!"
  body' =
    "Hi "
      ++ name c
      ++ ",\n\n"
      ++ "Since you've recently turned "
      ++ show (age c)
      ++ ", have you checked out our latest "
      ++ intercalate ", " (interestedIn c)
      ++ " products? Give us a visit!"

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

type API
  = "position" :> Capture "x" Int :> Capture "y" Int :> Get '[JSON] Position
  :<|> "hello" :> QueryParam "name" String :> Get '[JSON] HelloMessage
  :<|> "smart-hello" :> QueryParam' '[Required, Strict] "name" String :> Get '[JSON] HelloMessage
  :<|> "marketing" :> ReqBody '[JSON] ClientInfo :> Post '[JSON] Email

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

server :: Server API
server = position :<|> hello :<|> smartHello :<|> marketing
 where
  position :: Int -> Int -> Handler Position
  position x y = return $ Position x y

  hello :: Maybe String -> Handler HelloMessage
  hello mname = return . HelloMessage $ case mname of
    Nothing -> "Hello, anonymous coward"
    Just [] -> "Hello, tricky bastard"
    Just n  -> "Hello, " ++ n

  smartHello :: String -> Handler HelloMessage
  smartHello name = return $ HelloMessage $ "Hello, " ++ name

  marketing :: ClientInfo -> Handler Email
  marketing = return . emailForClient

api :: Proxy API
api = Proxy

app :: Application
app = serve api server

-- }}}
