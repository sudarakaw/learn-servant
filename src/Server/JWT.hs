{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}

module Server.JWT where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.ByteString                ( ByteString )
import           Data.Map                      as Map
import           GHC.Generics
import           Network.HTTP.Client            ( defaultManagerSettings
                                                , newManager
                                                )
import           Network.Wai.Handler.Warp
import           Servant
import           Servant.Auth                  as Auth
import           Servant.Auth.Server           as AuthServer
import           Servant.Client
import           System.IO

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

data AuthUser = AuthUser
  { userId :: Int
  , orgId  :: Int
  }
  deriving (Show, Generic)

instance ToJSON AuthUser
instance FromJSON AuthUser
instance ToJWT AuthUser
instance FromJWT AuthUser

type Login = ByteString
type Password = ByteString
type DB = Map (Login, Password) AuthUser
type Connection = DB
type Pool a = a

initConnectionPool :: IO (Pool Connection)
initConnectionPool = return $ fromList
  [(("user", "pass"), AuthUser 1 1), (("user2", "pass2"), AuthUser 2 1)]


type instance BasicAuthCfg = BasicAuthData -> IO (AuthResult AuthUser)

instance FromBasicAuthData AuthUser where
  fromBasicAuthData authData authCheckFunction = authCheckFunction authData

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

type API
  = "foo" :> Capture "i" Int :> Get '[JSON] ()
  :<|> "bar" :> Get '[JSON] ()

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

authCheck :: Pool Connection -> BasicAuthData -> IO (AuthResult AuthUser)
authCheck connPool (BasicAuthData login password) =
  return $ maybe Indefinite Authenticated $ Map.lookup (login, password)
                                                       connPool

type APIServer = Auth '[JWT, AuthServer.BasicAuth] AuthUser :> API

type TestAPIClient = Servant.BasicAuth "test" AuthUser :>API

testClient :: IO ()
testClient = do
  mgr <- newManager defaultManagerSettings

  let (foo :<|> _) =
        client (Proxy :: Proxy TestAPIClient) (BasicAuthData "name" "pass")

  res <- runClientM (foo 42)
                    (mkClientEnv mgr (BaseUrl Http "127.0.0.1" 5000 ""))

  hPutStrLn stderr $ case res of
    Left  err -> "Error: " ++ show err
    Right r   -> "Success: " ++ show r

server :: Server APIServer
server (Authenticated user) = foo :<|> bar
 where
  foo :: Int -> Handler ()
  foo n =
    liftIO $ hPutStrLn stderr $ concat ["foo: ", show user, " / ", show n]

  bar :: Handler ()
  bar = liftIO testClient
server _ = throwAll err401

app :: Pool Connection -> IO Application
app connPool = do
  myKey <- generateKey

  let jwtCfg  = defaultJWTSettings myKey
      authCfg = authCheck connPool
      cfg     = jwtCfg :. defaultCookieSettings :. authCfg :. EmptyContext
      api     = Proxy :: Proxy APIServer

  return $ serveWithContext api cfg server

run :: IO ()
run = do
  connPool <- initConnectionPool

  let settings = setPort 5000 $ setBeforeMainLoop
        (hPutStrLn stderr "listening on port 5000")
        defaultSettings

  runSettings settings =<< app connPool

-- }}}
