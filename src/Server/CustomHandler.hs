{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}


module Server.CustomHandler where

-- IMPORTS ----------------------------------------------------------------- {{{

-- import           Control.Monad.Reader
import           Servant

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

type API
  = "a" :> Get '[JSON] Int
  :<|> "b" :> ReqBody '[JSON] Double :> Get '[JSON] Bool

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

api :: Proxy API
api = Proxy

-- readerToHandler :: Reader Double a -> Handler a
-- readerToHandler r = return $ runReader r 33
--
-- serverT :: ServerT API (Reader Double)
-- serverT = a :<|> b
--  where
--   a :: Reader Double Int
--   a = return 1797
--
--   b :: Double -> Reader Double Bool
--   b val = asks (== val)
--
-- server :: Server API
-- server = hoistServer api readerToHandler serverT

fToHandler :: (Double -> a) -> Handler a
fToHandler f = return $ f 33

fServerT :: ServerT API ((->) Double)
fServerT = a :<|> b
 where
  a :: Double -> Int
  a _ = 1797

  b :: Double -> Double -> Bool
  b val input = input == val

fserver :: Server API
fserver = hoistServer api fToHandler fServerT

app :: Application
app = serve api fserver

-- }}}
