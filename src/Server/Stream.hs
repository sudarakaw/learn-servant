{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DeriveGeneric #-}

module Server.Stream where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Data.Aeson
import           Data.Time.Calendar
import           GHC.Generics
import           Servant
import           Servant.Types.SourceT          ( source )

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

data User = User
  { name              :: String
  , age               :: Int
  , email             :: String
  , registration_date :: Day
  }
  deriving Generic

instance ToJSON User


albert :: User
albert = User "Albert Einstein" 136 "ae@mc2.org" (fromGregorian 1905 12 1)

issac :: User
issac = User "Isaac Newton" 372 "isaac@newton.co.uk" (fromGregorian 1683 3 1)

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

type API
  = "user-stream" :> StreamGet NewlineFraming JSON (SourceIO User)
  :<|> "int-stream" :> StreamGet NewlineFraming JSON (SourceIO Int)

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

streamUsers :: SourceIO User
streamUsers = source [issac, albert, albert, issac, issac, albert]

streamInt :: SourceIO Int
streamInt = source [1 ..]

api :: Proxy API
api = Proxy

server :: Server API
server = return streamUsers :<|> return streamInt

app :: Application
app = serve api server

-- }}}
