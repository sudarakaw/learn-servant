{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}


module Server.Static where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Servant

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

type API = "static" :> Raw

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

api :: Proxy API
api = Proxy

server :: Server API
server = serveDirectoryWebApp "."

app :: Application
app = serve api server

-- }}}
