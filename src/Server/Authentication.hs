{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeOperators         #-}

module Server.Authentication where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Data.Aeson
import           Data.Text                      ( Text )
import           GHC.Generics
import           Servant

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

-- | private data that needs protection
newtype PrivateData = PrivateData {ssshhh::Text}
  deriving Generic

instance ToJSON PrivateData

-- | public data that anyone can use.
newtype PublicData = PublicData { somedata :: Text }
  deriving Generic

instance ToJSON PublicData

-- | A user we'll grab from the database when we authenticate someone
newtype User = User { userName :: Text }

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

-- | a type to wrap our public api
type PublicAPI = Get '[JSON] [PublicData]

-- | a type to wrap our private api
type PrivateAPI = Get '[JSON] PrivateData


type API
  = "public" :> PublicAPI
  :<|> "private" :> BasicAuth "foo-realm" User :> PrivateAPI

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

-- | 'BasicAuthCheck' holds the handler we'll use to verify a username and password.
authCheck :: BasicAuthCheck User
authCheck =
  let check (BasicAuthData username password) =
        if username == "servant" && password == "server"
          then return $ Authorized $ User "servant"
          else return Unauthorized
  in  BasicAuthCheck check

authContext :: Context (BasicAuthCheck User ': '[])
authContext = authCheck :. EmptyContext

api :: Proxy API
api = Proxy

-- | an implementation of our server. Here is where we pass all the handlers to our endpoints.
-- In particular, for the BasicAuth protected handler, we need to supply a function
-- that takes 'User' as an argument.
server :: Server API
server =
  let publicAPIHandler = return [PublicData "foo", PublicData "bar"]
      privateAPIHandler (user :: User) = return $ PrivateData $ userName user
  in  publicAPIHandler :<|> privateAPIHandler

app :: Application
app = serveWithContext api authContext server

-- }}}
