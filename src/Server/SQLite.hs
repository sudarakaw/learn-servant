{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module Server.SQLite where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Control.Monad.IO.Class
import           Database.SQLite.Simple
import           Servant
import           Servant.Client

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

type Message = String

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

type API
  =  ReqBody '[PlainText] Message :> Post '[JSON] NoContent
  :<|> Get '[JSON] [Message]

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

api :: Proxy API
api = Proxy

initDB :: FilePath -> IO ()
initDB dbfile = withConnection dbfile $ \conn ->
  execute_ conn "CREATE TABLE IF NOT EXISTS messages (msg text not null)"

server :: FilePath -> Server API
server dbfile = postMessage :<|> getMessage
 where
  getMessage :: Handler [Message]
  getMessage = fmap (map fromOnly) . liftIO $ withConnection dbfile $ \conn ->
    query_ conn "SELECT msg FROM messages"

  postMessage :: Message -> Handler NoContent
  postMessage msg = do
    liftIO . withConnection dbfile $ \conn ->
      execute conn "INSERT INTO messages VALUES (?)" (Only msg)

    return NoContent

app :: FilePath -> Application
app dbfile = serve api $ server dbfile

-- }}}
