{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Server.Doc where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Data.ByteString.Lazy           ( ByteString )
import           Data.Text.Lazy
import           Data.Text.Lazy.Encoding
import           Network.HTTP.Types
import           Network.Wai
import           Servant
import           Servant.Docs                   ( ParamKind(..)
                                                , ToCapture
                                                , ToParam
                                                , ToSample
                                                , docs
                                                , docsWithIntros
                                                , markdown
                                                , singleSample
                                                )
import qualified Servant.Docs                  as D
import           Server.API

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

instance ToCapture (Capture "x" Int) where
  toCapture _ = D.DocCapture "x" --name
                             "(integer) position on the x axis" -- description

instance ToCapture (Capture "y" Int) where
  toCapture _ = D.DocCapture "y" --name
                             "(integer) position on the y axis" -- description

instance ToSample Position where
  toSamples _ = singleSample $ Position 3 14

instance ToParam (QueryParam "name" String) where
  toParam _ = D.DocQueryParam "name"  -- name
                              ["Alp", "John Doe", "..."]  -- example of values (not necessarily exhaustive)
                              "Name of the person to say hello to." -- description
                              Normal  -- Normal, List or Flag

instance ToParam (QueryParam' '[Required, Strict] "name" String) where
  toParam _ = D.DocQueryParam "name"  -- name
                              ["Alp", "John Doe", "..."]  -- example of values (not necessarily exhaustive)
                              "Name of the person to say hello to." -- description
                              Normal  -- Normal, List or Flag

instance ToSample HelloMessage where
  toSamples _ =
    [ ("When a value is provided for 'name'", HelloMessage "Hello, Alp")
    , ("When 'name' is not specified", HelloMessage "Hello, anonymous coward")
    ]
    -- multiple examples to display this time

ci :: ClientInfo
ci = ClientInfo "Alp" "alp@foo.com" 26 ["haskell", "mathematics"]

instance ToSample ClientInfo where
  toSamples _ = singleSample ci

instance ToSample Email where
  toSamples _ = singleSample $ emailForClient ci

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

type DocsAPI = API :<|> Raw

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

apiDocs :: D.API
apiDocs = docs api

docsBS :: ByteString
docsBS = encodeUtf8 . pack . markdown $ docsWithIntros [intro] api
 where
  intro =
    D.DocIntro "Welcome" ["This is our super webservice's API.", "Enjoy!"]

docsApi :: Proxy DocsAPI
docsApi = Proxy

docsServer :: Server DocsAPI
docsServer = Server.API.server :<|> Tagged serveDocs where
  serveDocs _ respond = respond $ responseLBS ok200 [plain] docsBS
  plain = ("Content-Type", "text/plain")

app :: Application
app = serve docsApi docsServer

-- }}}
