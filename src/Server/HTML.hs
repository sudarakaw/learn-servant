{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}


module Server.HTML where

-- IMPORTS ----------------------------------------------------------------- {{{

import           Data.Aeson
import           GHC.Generics
import           Lucid.Base
import           Lucid.Html5
import           Servant
import           Servant.HTML.Lucid

-- }}}

-- MODEL ------------------------------------------------------------------- {{{

data Person = Person
  { firstName :: String
  , lastName  :: String
  }
  deriving Generic

instance ToJSON Person

-- }}}

-- ROUTES ------------------------------------------------------------------ {{{

type PersonAPI = "persons" :> Get '[JSON, HTML] [Person]

-- }}}

-- VIEW -------------------------------------------------------------------- {{{

instance ToHtml Person where
  toHtml person = tr_ $ do
    td_ (toHtml $ firstName person)
    td_ (toHtml $ lastName person)

  toHtmlRaw = toHtml

instance ToHtml [Person] where
  toHtml persons = table_ $ do
    tr_ $ do
      th_ "First Name"
      th_ "Last Name"

    foldMap toHtml persons

  toHtmlRaw = toHtml

-- }}}

-- SERVER ------------------------------------------------------------------ {{{

people :: [Person]
people = [Person "Isaac" "Newton", Person "Albert" "Einstein"]

personAPI :: Proxy PersonAPI
personAPI = Proxy

server :: Server PersonAPI
server = return people

app :: Application
app = serve personAPI server

-- }}}
